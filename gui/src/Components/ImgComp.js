import React, { Component } from 'react'
import BboxComp from './BboxComp'
import SegmentGraph from './SegmentGraph'
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import logo from './logo.png';
import axios from 'axios';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import IconButton from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import Switch from '@material-ui/core/Switch';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import AddIcon from '@material-ui/icons/Add';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


const styles = theme =>({
    root: {
      },
      formControl: {
        margin: 25,
        width: 421,
      },
      picture:{
          marginTop: theme.spacing(8),
          width: '70vw',
          height:'60vw',
      },

      textField:{
        marginTop: theme.spacing(2),
          width: '80%',
          
      },

      
})

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

export class ImgComp extends Component {
    constructor(props){
        super(props);
        this.state = {
            clickedOut: false,
            showSeg: false,
            width: 900,
            height: 900,
            bbox:[],
            loggedIn:false,
            url: 'http://127.0.0.1:10081/',
            email:'',
            username: '',
            Password: '',
            Password1: '',
            Password2: '',
            token: '',
            xray_img:'',
            models:[],
            diseases:[],
            segment_array:Array(),
            model_name: "",
            xid:"",
            segment:"",
            showSnacksuccesss:false,
            showSnackerror:false,
            showAddDisease:false,
            new_disease:''
        }
        this.setCord = this.setCord.bind(this)
        this.setSize = this.setSize.bind(this)
        this.setName = this.setName.bind(this)
        this.clickIn = this.clickIn.bind(this)
        this.setSegmentCord = this.setSegmentCord.bind(this)
    }

    getData = () => {
        this.getModels();
        this.getDiseases();
        axios.get(`${this.state.url}api/show_xray`, 
        Headers={Authorization:`Token ${this.state.token}`})
        .then(res=>{
            if(res.data[0] === undefined){
                console.log("Nooooo");
            }
            else{
                console.log(res.data)
                let bboxes = res.data[0].bboxes;
                let xray = res.data[0].picture;
                let seg = res.data[0].segment;
                let xray_id = res.data[0].id;
                bboxes.map(bbox=>{
                    let w = this.state.width;
                    let h = this.state.height;
                    console.log(bbox);
                    let x = bbox['xmin']*w;
                    let y = bbox['ymin']*h;
                    let bbw = (bbox['xmax'] - bbox['xmin'])*w;
                    let bbh = (bbox['ymax'] - bbox['ymin'])*h;
                    let name = bbox['disease'];
                    bbox['x'] = x;
                    bbox['y'] = y;
                    bbox['bbw'] = bbw;
                    bbox['bbh'] = bbh;
                    bbox['name'] = name;
            })
            this.setState({bbox:bboxes, xray_img:xray, segment:seg, xid:xray_id})
            }
        })
    }

    clickIn = () =>{
        this.setState({
            clickedOut: false,
        })
    }

    downloadCSV = () =>{
        if(this.state.showSeg){
            fetch(`${this.state.url}api/get_segment_csv`,Headers={Authorization:`Token ${this.state.token}`})
			.then(response => {
				response.blob().then(blob => {
					let url = window.URL.createObjectURL(blob);
					let a = document.createElement('a');
					a.href = url;
					a.download = 'segmentation_annotations.csv';
					a.click();
				});
		    });
            return;
        }
        fetch(`${this.state.url}api/get_csv`,Headers={Authorization:`Token ${this.state.token}`})
			.then(response => {
				response.blob().then(blob => {
					let url = window.URL.createObjectURL(blob);
					let a = document.createElement('a');
					a.href = url;
					a.download = 'bounding_box_annotations.csv';
					a.click();
				});
		});
    }

    setSegmentCord = (arr) => {
        //console.log(arr)
        let segment_arr = Array();
        for(let i = 0; i < arr.length; i++){
            if(arr[i]=='break'){
                continue;
            }
            if(i > 0 && arr[i-1]=='break'){
                continue;
            }
            segment_arr.push(arr[i]);
        }
        this.setState({segment_array:segment_arr})
    }

    setCord = (x, y, key) => {
        let tempState = this.state.bbox
        if(x > this.state.width || y > this.state.height || x < (-1*this.state.bbox[key].bbw)){
            tempState.splice(key, key + 1);
            this.setState({bbox:tempState})
        }
        else{
            tempState[key].x = x;
            tempState[key].y = y;
            this.setState({bbox:tempState})
        }
    }

    setSize = (x, y, key) => {
        let tempState = this.state.bbox
        tempState[key].bbw = x;
        tempState[key].bbh = y;
        this.setState({bbox:tempState})
    }

    setName = (name, key) => {
        let tempState = this.state.bbox
        tempState[key].name = name
        this.setState({bbox:tempState})
    }

    logIn = () => {
        axios.post(`${this.state.url}rest-auth/login/`, {
            username: this.state.username,
            password: this.state.password,
        }).then(res=>{
            this.setState({token:res.data.key, loggedIn:true,});
        })
    }

    signUp = () => {
        axios.post(`${this.state.url}rest-auth/registration/`, {
            username: this.state.username,
            email: this.state.email,
            password1: this.state.password1,
            password2: this.state.password2,
        }).then(res=>{
            this.setState({token:res.data.key, loggedIn:true,});
        })
    }

    getBoxData = () => {
        this.getModels();
        if(this.state.model_name===""){
            console.log("Select a model")
            return 1;
        }

        if(this.state.xray_img===""){
            console.log("get an xray")
            return 1;
        }

        
        let id = this.state.xid;
        console.log(id);
        axios.get(`${this.state.url}api/get_bbox/${id}?model=${this.state.model_name}`, 
        Headers={Authorization:`Token ${this.state.token}`})
        .then(res=>{
            let bboxes = res.data;
            bboxes.map(bbox=>{
                let w = this.state.width;
                let h = this.state.height;
                console.log(bbox);
                let x = bbox['xmin']*w;
                let y = bbox['ymin']*h;
                let bbw = (bbox['xmax'] - bbox['xmin'])*w;
                let bbh = (bbox['ymax'] - bbox['ymin'])*h;
                let name = bbox['disease'];
                bbox['x'] = x;
                bbox['y'] = y;
                bbox['bbw'] = bbw;
                bbox['bbh'] = bbh;
                bbox['name'] = name;
            })
            this.setState({bbox:bboxes})
        })
    }

    getModels = () => {
        axios.get(`${this.state.url}api/get_dl_model`, 
        Headers={Authorization:`Token ${this.state.token}`})
        .then(res => {
            this.setState({models:res.data})
        })
    }

    sendHumanBboxes = () => {
        if(this.state.showSeg){
            //console.log(this.state.segment_array);
            let seg_arr = this.state.segment_array;
            let segment_points = "";
            for(let i = 0; i < seg_arr.length; i++){
                
                let x = `${seg_arr[i][0]}, `;
                let y = `${seg_arr[i][1]}, `;
                segment_points = segment_points + x + y;
            }
            let segment_obj = {points:segment_points, xray:this.state.xid};

            console.log(segment_points)
            axios.post(`${this.state.url}api/segmentation`,
            segment_obj,
            {headers:{Authorization:`Token ${this.state.token}`}},
            )
            .then(res => {
            this.setState({showSnacksuccesss:true, showSeg:false})
            })
            .catch(e => {
                this.setState({showSnackerror:true})
            })
            return;
        }
        let boxes = []
        let h = this.state.height;
        let w = this.state.width;

        let id = 0

        id = this.state.xid;

        this.state.bbox.map(box=>{
            let obj = {}
            obj["xmin"] = box.x/w;
            obj["ymin"] = box.y/h;
            obj["xmax"] = (box.x + box.bbw)/w;
            obj["ymax"] = (box.y + box.bbh)/h;
            obj["disease"] = box.name;
            obj["dl_model"] = "Human";
            boxes.push(obj);
        })

        axios.patch(`${this.state.url}api/update_bbox/${id}`,
        {bboxes:boxes},
        {headers:{Authorization:`Token ${this.state.token}`}},
        )
        .then(res => {
          this.setState({showSnacksuccesss:true})
        })
        .catch(e => {
            this.setState({showSnackerror:true})
        })
    }

    componentDidUpdate = () => {
        //console.log(this.state);
    }

    componentDidMount = () => {
        this.getModels();
        this.getDiseases()
    }

    getDiseases = () => {
        axios.get(`${this.state.url}api/get_diseases`,
        {headers:{Authorization:`Token ${this.state.token}`}},
        )
        .then(res => {
            //console.log(res)
            this.setState({diseases:res.data})
        })
    }

    addDisease = (disease_name) => {
        //this.setState(prevState => ({diseases:[...prevState.diseases, disease_name]}))
        axios.post(`${this.state.url}api/post_logs`,
        {disease: disease_name},
        {headers:{Authorization:`Token ${this.state.token}`}},
        )
        .then(res => {
            this.setState({showAddDisease:false, new_disease:''})
            this.getDiseases()
        })
        .catch(err => {
            this.setState({showSnackerror:true})
        })
    }

    render() {
        const { classes} = this.props;

        let Bboxes = this.state.bbox.map((bbox, indx)=>{
            
            return(
                <BboxComp setName={this.setName} diseases={this.state.diseases} clickIn={this.clickIn} clickedOut={this.state.clickedOut} id={indx} key={indx} setSize={this.setSize} setCord={this.setCord} cords={bbox}/>
            )
        })
        
        return (
            <div style={{display:'flex',
            justifyContent:'center',
            }}>
            {
            this.state.loggedIn?
            <>
            {
            this.state.showSeg?
            <div style={{marginTop:'40px', position:'relative', height:this.state.height, width:this.state.width, backgroundImage:`url(${this.state.segment})`, backgroundColor:'black', backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundPosition: "center center" , zIndex:0}}>
                <SegmentGraph setCoord={this.setSegmentCord}  height={this.state.height} width={this.state.width} />
            </div>
            :
            <div
                onClick={(e)=>{
                e.stopPropagation();
                console.log("Clicked on Black area")
                
                this.setState({clickedOut: true})}} style={{marginTop:'40px', position:'relative', height:this.state.height, width:this.state.width, backgroundImage:`url(${this.state.xray_img})`, backgroundColor:'black', backgroundSize: "cover", backgroundRepeat: "no-repeat", backgroundPosition: "center center" , zIndex:0}}>
                
            {Bboxes}

            <>
                    {
                        this.showAddDisease?
                        <div>

                        </div>
                        :
                        <div
                        style={{position: 'absolute', marginTop:`${this.state.height-100}px`, marginLeft:`${this.state.width-100}px`}}
                        >
                            <Tooltip title="add new disease label to the set of labels">
                            <IconButton
                            variant="contained"
                            color="secondary"
                            onClick={()=>this.setState({showAddDisease:true})}
                            style={{height:74, width:74, borderRadius:'50%'}}
                            >
                                <AddIcon/>
                            </IconButton>
                            </Tooltip>
                        </div>
                    }
            </>

            </div>
                }
            <Paper
            style={{
                display:'flex',
                justifyContent:'center',
                height:this.state.height,
                marginTop:'40px',
                width:500,
                backgroundColor:'rgba(255, 255, 255, .15)',
                backdropFilter: 'blur(10px)',
            }}
            >
            <div>
            <Grid container justify="center" spacing={2} direction="column" alignItems="center">
            <img style={{marginTop: 40, width: 400}} src={logo}></img>
            
            {
            this.state.showSeg?
            <>
            </>
            :
            <>
            <Tooltip title="Add bounding boxes to the image">
            <Button
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>{
                let x = Math.trunc(Math.random()*300);
                let y = Math.trunc(Math.random()*300);

                let bbw  = 300;
                let bbh =  300;

                let arr = this.state.bbox;

                let name = 'bbox';
                arr.push({name, bbh, bbw, x, y})
                this.setState({bbox:arr})
                
            }}
            
            style={{height:70, width: 420, marginTop:40}}>Add Bounding Box</Button>
            </Tooltip>
            <Tooltip title="select your desired model">
            <FormControl variant="outlined" className={classes.formControl}>
                    <InputLabel id="demo-simple-select-outlined-label">ML Model</InputLabel>
                <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                label="Age"
                onChange={(e)=>{
                    this.setState({"model_name":this.state.models[e.target.value].name})
                }}
                >
                {
                    this.state.models.map((model, indx)=>(
                    <MenuItem value={indx}>{model["name"]}</MenuItem>
                    ))
                }
                </Select>
            </FormControl>
            </Tooltip>
            <Tooltip title="get bounding of the selected model">
            <Button
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>this.getBoxData()}
            style={{height:70, width: 420}}
            >Get bbox</Button>
            </Tooltip>
            </>
            }
            
            <div style={{marginTop:40}}>
                Segmentation
            </div>
            <Tooltip title="switch segmentation">
            <Switch
            checked={this.state.showSeg}
                onChange={()=>this.setState(prevState=>({showSeg : !prevState.showSeg}))}
                color="secondary"
                name="segmentaion"
                inputProps={{ 'aria-label': 'primary checkbox' }} />
            </Tooltip>

            <Tooltip title="get new image for annotations">
            <Button
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>this.getData()}
            style={{height:70, width: 420, marginTop: 35}}
            >Get Image</Button>
            </Tooltip>

            <Tooltip title="save the modified bounding boxes to the backend">  
            <Button
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>this.sendHumanBboxes()}
            style={{height:70, width: 420, marginTop: 35}}
            >
                SAVE
            </Button>
            </Tooltip>

            <Tooltip title="Download csv"> 
            
            <IconButton
            variant="contained"
            color="secondary"
            onClick={()=>this.downloadCSV()}
            style={{marginTop:35, height:74, width:74, borderRadius:'50%'}}
            >
                <CloudDownloadIcon/>
            </IconButton>
            
            </Tooltip>
            </Grid>
            </div>
            </Paper>
            </>
            :
            <>
            <div
            style={{
                height: 900,
                marginTop:'50px',
                width: 500,
                backgroundColor:'rgba(255, 255, 255, .08)',
                backdropFilter: 'blur(20px)',
                borderRadius: '5px',
            }}
            >
            <Grid container justify="center" spacing={2} direction="column" alignItems="center">
            <img style={{marginTop: 20, marginBottom: 5, width: 400}} src={logo}></img>
            
            <TextField
            id="outlined"
            label="Username"
            defaultValue=""
            variant="outlined"
            color="secondary"
            className={classes.textField}
            onChange={(e)=>{this.setState({username:e.target.value})}}
            >  </TextField>

            <TextField
            id="outlined-password-input"
            label="Password"
            type="password"
            autoComplete="current-password"
            variant="outlined"
            className={classes.textField}
            onChange={(e)=>{this.setState({password:e.target.value})}}
            color="secondary"
            />
            
            <Button 
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>{
                this.logIn();
            }}
            style={{height:59, width: "80%", marginTop:20}}>Log in</Button>
            
            <div style={{color:"#bcbfc2", fontSize: '2em', marginTop: 50}}>
            SIGN UP
            </div>

            <TextField
            id="outlined"
            label="Username"
            defaultValue=""
            variant="outlined"
            color="secondary"
            className={classes.textField}
            onChange={(e)=>{this.setState({username:e.target.value})}}
            /> 

            <TextField
            id="outlined"
            label="Email"
            type="email"
            defaultValue=""
            variant="outlined"
            color="secondary"
            className={classes.textField}
            onChange={(e)=>{this.setState({email:e.target.value})}}
            /> 

            <TextField
            id="outlined-password-input"
            label="Password1"
            type="password"
            autoComplete="current-password"
            variant="outlined"
            className={classes.textField}
            onChange={(e)=>{this.setState({password1:e.target.value})}}
            color="secondary"
            />

            <TextField
            id="outlined-password-input"
            label="Password2"
            type="password"
            autoComplete="current-password"
            variant="outlined"
            className={classes.textField}
            onChange={(e)=>{this.setState({password2:e.target.value})}}
            color="secondary"
            />

            
            <Button 
            variant="contained"
            size="large"
            color="secondary"
            onClick={()=>{
                this.signUp()
            }}
            style={{height:59, width: "80%", marginTop:20}}>Sign Up</Button>
            </Grid>
            </div>
            
            
            </>
            }
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={this.state.showSnacksuccesss}
                autoHideDuration={6000}
                onClose={()=>this.setState({showSnacksuccesss:false})}
                action={
                <React.Fragment>
                    <IconButton size="medium" aria-label="close" color="primary" onClick={()=>this.setState({showSnacksuccesss:false})}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={()=>this.setState({showSnacksuccesss:false})} severity="success">
                    Saved succesfully
                </Alert>
            </Snackbar>
            <Snackbar
                anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
                }}
                open={this.state.showSnackerror}
                autoHideDuration={6000}
                onClose={()=>this.setState({showSnackerror:false})}
                action={
                <React.Fragment>
                    <IconButton size="medium" aria-label="close" color="primary" onClick={()=>this.setState({showSnackerror:false})}>
                    <CloseIcon fontSize="small" />
                    </IconButton>
                </React.Fragment>
                }
            >
                <Alert onClose={()=>this.setState({showSnackerror:false})} severity="error">
                    Somthing went wrong
                </Alert>
            </Snackbar>
                <Dialog open={this.state.showAddDisease} onClose={()=>this.setState({showAddDisease:false})} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add Disease</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        You are adding a new disease to the set of diseases. Please check once again if the
                        disease label is already available.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        label="disease"
                        type="text"
                        variant="outlined"
                        color='secondary'
                        className={classes.textField}
                        onChange={(e)=>{this.setState({new_disease:e.target.value})}}
                        fullWidth
                    />
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={()=>this.setState({showAddDisease:false})} color="secondary">
                        Cancel
                    </Button>
                    <Button onClick={()=>this.addDisease(this.state.new_disease)} color="secondary">
                        Add Disease
                    </Button>
                    </DialogActions>
                </Dialog>
    
            </div>
            
        )
    }
}

ImgComp.propTypes = {
    classes: PropTypes.object.isRequired,
    open: PropTypes.bool.isRequired,
    value: PropTypes.number.isRequired,
}

export default withStyles(styles)(ImgComp);
