import React, { useState, useEffect} from 'react';
import IconButton from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import AutorenewIcon from '@material-ui/icons/Autorenew';


let dfs = (edges, src, visited, n) => {
    visited[src] = 1;
    //draw this src node at its respective co-ordinate;
    for(let i = 0; i < n; i++){
        if(!visited[i] && edges[src][i]){
            dfs(edges, i, visited, n);
        }
    }
}

let dist_from_init_point = (x1, y1, x2, y2) => {
    return ((x1-x2)**2+(y1-y2)**2)**0.5
}

let draw_circle = (ctx, cx, cy) => {
    ctx.beginPath();
    ctx.arc(cx, cy, 5, 0, 2*Math.PI, false);
    ctx.fillStyle = '#f59031';
    ctx.fill();
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#e65100';
    ctx.stroke();
}

let join_dots = (ctx, x1, y1, x2, y2) => {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.strokeStyle = '#e65100';
    ctx.lineWidth = 3;
    ctx.stroke();
}

const SegmentGraph = (props) => {
    const [nodes, setNode] = useState([]);

    useEffect(()=>{
        let canvas = document.getElementById('canvas');
        canvas.height = props.height;
        canvas.width = props.width;
        canvas.style.backgroundColor = 'rgba(0,0,0,0)';
        let ctx = canvas.getContext('2d');
        let prev = [0, 0];
        for(let i = 0; i < nodes.length; i++){

            if(nodes[i]=="break"){
                prev = "break";
                continue;
            }

            if(prev=="break"){
                draw_circle(ctx, nodes[i][0], nodes[i][1]);
                prev = nodes[i];
                continue;
            }

            if(i==0){
                draw_circle(ctx, nodes[i][0], nodes[i][1]);
                //set prev
                prev = nodes[i]
                //continue
                continue;
            }
            
            //draw the next circle
            draw_circle(ctx, nodes[i][0], nodes[i][1]);
            //join the the dot with prev circle
            join_dots(ctx, prev[0], prev[1], nodes[i][0], nodes[i][1]);
            prev = nodes[i];
            
        }
        
    })
    return (
        <>
        <canvas
        style={{position:'absolute', zIndex:2}}
        onClick={e=>{
            
            let rect = e.target.getBoundingClientRect();
            let x = e.clientX - rect.left;
            let y = e.clientY - rect.top;
            if(nodes.length){
                for(let i = 1; i < nodes.length; i++){
                    if(nodes[i-1]=="break"){
                        let init_x = nodes[i][0];
                        let init_y = nodes[i][1];
                        if(dist_from_init_point(init_x, init_y, x, y)<6){
                            setNode([...nodes, [init_x, init_y], "break"])
                            props.setCoord(nodes);
                            return;
                        }
                    }
                }
                let init_x = nodes[0][0];
                let init_y = nodes[0][1];
                if(dist_from_init_point(init_x, init_y, x, y)<6){
                    setNode([...nodes, [init_x, init_y], "break"])
                    props.setCoord(nodes);
                    return;
                }
            }
            setNode([...nodes, [x, y]])
            props.setCoord(nodes);
        }}
        id="canvas"></canvas>
        <div style={{position: 'absolute', marginTop:`${props.height-100}px`, marginLeft:`${props.width-100}px`, zIndex:100}}>
            <Tooltip title="remove all the segment points">
            <IconButton
            variant="contained"
            color="secondary"
            onClick={()=>{setNode([]);
                props.setCoord(nodes);
            }}
            style={{height:74, width:74, borderRadius:'50%'}}
            >
                <AutorenewIcon/>
            </IconButton>
            </Tooltip>
        </div>
        </>
    );
};

export default SegmentGraph;