import React from 'react';
import ImgComp from './Components/ImgComp.js'
import background from './background.svg'



function App() {
  return (
    <>
    <img style={{
      position:'absolute',
      width:'100vw',
      height:'100vh',
      objectFit: 'cover',
      /* background by SVGBackgrounds.com */
    }} src={background}></img>
    <ImgComp/>
    </>
  );
}

export default App;
