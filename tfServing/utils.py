"""This module provides utility functions for parsing jsons and making requests"""
import json
import requests_async as requests

def get_disease_name(num):
    num = int(num)
    if(num==1):
        return 'Cardiomegaly'
    if(num==2):
        return 'Effusion'
    if(num==3):
        return 'Pneumothorax'
    if(num==4):
        return 'Atelectasis'
    if(num==5):
        return 'Mass'
    if(num==7):
        return 'Nodule'
    else:
        return 'Pneumonia'
    

def get_bboxes(bbox_lists, class_list, model_name):
    """
    This function is responsible for bbox array into dict for
    sending it into json format

    Args:
        bbox_lists (list): list of lists of bboxes corresponding to an img
        class_list (list): list of class of disease corresponding to a bbox

    Returns:
        dict: dictionary with bounding box info
    """
    bbox_dict = {"bboxes":[]}
    for i, bbox in enumerate(bbox_lists):
        print((bbox, model_name))
        bbox_dict["bboxes"].append({"xmin":bbox[0],
                                    "ymin":bbox[1],
                                    "xmax":bbox[2],
                                    "ymax":bbox[3],
                                    "disease":get_disease_name(class_list[i]),
                                    "dl_model":model_name
                                    })
    return bbox_dict

async def add_xray(payload_list):
    """
    This function is responsible for sending the data to the django backend

    Args:
        payload (dict): dictionary that consists of image and its bbox data
    """
    for payload in payload_list:
        headers = {
            "content-type":"application/json",
            "Authorization":"Token db42c52552cdfc16a7e9b822758ca19391beaa81"
        }
        json_response = await requests.post('http://127.0.0.1:10081/api/add_xray',
                                            data=json.dumps(payload),
                                            headers=headers)
        #print(json_response.json())