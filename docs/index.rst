.. lh-semi-automatic-annotation documentation master file, created by
   sphinx-quickstart on Mon Aug 24 16:04:10 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lh-semi-automatic-annotation's documentation!
========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
